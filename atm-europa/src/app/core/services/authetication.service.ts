import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {UserLogin} from '../models/user-login';
import {UserSession} from '../models/user-session';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  userSession: UserSession;

  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json', 'Access-Control-Allow-Origin': 'localhost:4200/*'});

  constructor(
    private httpClient: HttpClient
  ) {
  }

  authentication(data: UserLogin): Observable<any> {
    return this.httpClient.post(environment.serviceUrl + 'api/login', data, {observe: 'response'});
  }

  saveSession(dataAuth: UserSession) {
    sessionStorage.setItem('login', JSON.stringify(dataAuth));
  }

  cargarSession() {
    if (sessionStorage.getItem('login')) {
      this.userSession = JSON.parse(sessionStorage.getItem('login'));
    }
  }
}
