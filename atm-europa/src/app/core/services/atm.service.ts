import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Atm} from '../models/atm';
import {Address} from '../models/address';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AtmService {

  address: Address[];

  constructor(
    private httpClient: HttpClient
  ) {
  }

  getList(): Observable<Atm[]> {
    return this.httpClient.get<Atm[]>(environment.serviceUrl + 'atm/list');
  }

  getListWithFilter(atmFilter: Atm): Observable<Atm[]> {
    return this.httpClient.post<Atm[]>(environment.serviceUrl + 'atm/listWithFilter', atmFilter);
  }

}
