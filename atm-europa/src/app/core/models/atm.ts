import {Address} from './address';

export class Atm{
  address: Address;
  distance: string;
  type: string;
}
