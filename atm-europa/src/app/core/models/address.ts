import {GeoLocationAtm} from './geoLocationAtm';

export class Address{
  street: string;
  houseNumber: string;
  postalCode: string;
  city: string;
  geoLocation: GeoLocationAtm;
}
