import {NgModule} from "@angular/core";
import {PagesComponent} from "./pages.component";
import {HomeComponent} from "./home/home.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {SharedModule} from "../shared/shared.module";
import {PAGES_ROUTES} from "./pages.routes";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { RootNavComponent } from './root-nav/root-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {MatTableModule} from "@angular/material/table";
import {MatSortModule} from "@angular/material/sort";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatCardModule} from "@angular/material/card";
import {MatTabsModule} from "@angular/material/tabs";
import {MatCheckboxModule} from "@angular/material/checkbox";
import { MatGridListModule } from '@angular/material/grid-list';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import {MatDialogModule} from "@angular/material/dialog";
import { MapComponent } from './map/map.component';
import {GoogleMapsModule} from '@angular/google-maps';
import { AdvancedFilterComponent } from './atm/advanced-filter/advanced-filter.component';
import {ChartsModule} from 'ng2-charts';

@NgModule({
  declarations: [
    PagesComponent,
    HomeComponent,
    RootNavComponent,
    MapComponent,
    AdvancedFilterComponent,

  ],
  exports: [
    HomeComponent,
    FormsModule,
    ReactiveFormsModule,

  ],
  imports: [
    CommonModule,
    SharedModule,
    PAGES_ROUTES,
    FormsModule,
    ReactiveFormsModule,
    LayoutModule,
    MatToolbarModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatTabsModule,
    MatFormFieldModule,
    MatButtonModule,
    MatCheckboxModule,
    MatGridListModule,
    MatMenuModule,
    MatSelectModule,
    MatRadioModule,
    MatDialogModule,
    GoogleMapsModule,
    ChartsModule
  ]
})
export class PagesModule {
}
