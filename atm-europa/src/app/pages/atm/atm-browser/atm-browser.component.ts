import {Component, OnInit, ViewChild} from '@angular/core';
import {AtmService} from '../../../core/services/atm.service';
import {Atm} from '../../../core/models/atm';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {Address} from '../../../core/models/address';
import {MatDialog} from '@angular/material/dialog';
import {MapComponent} from '../../map/map.component';
import {Router} from '@angular/router';
import {AdvancedFilterComponent} from '../advanced-filter/advanced-filter.component';
import {GeoLocationAtm} from '../../../core/models/geoLocationAtm';

@Component({
  selector: 'app-atm-browser',
  templateUrl: './atm-browser.component.html',
  styleUrls: ['./atm-browser.component.css']
})
export class AtmBrowserComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  atms: Atm[];
  atmFilter: Atm = new Atm();
  atmNewFilter: Atm = new Atm();

  address: Address = new Address();
  geoLocationAtm: GeoLocationAtm = new GeoLocationAtm();
  tableData: MatTableDataSource<Atm>;
  searchKey: string;


  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns: string [] = ['type', 'street', 'distance', 'city', 'actions'];

  constructor(
    private atmService: AtmService,
    private dialog: MatDialog,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.loadData(this.atmFilter);
  }

  onSearchClear() {
    this.searchKey = '';
    this.applyFilter();
  }

  advancedFilter() {
    this.dialog.open(AdvancedFilterComponent, {data: null}).afterClosed().subscribe((result: Atm) => {
      if (result) {
        Object.assign(this.atmFilter, result);
        this.atmService.getListWithFilter(this.atmFilter).subscribe((list: Atm[]) => {
          this.atms = list;
          this.tableData = new MatTableDataSource(list);
          this.tableData.sort = this.sort;
          this.tableData.paginator = this.paginator;
        });
      }
    });
  }

  applyFilter() {
    if (this.searchKey != null) {
      this.tableData.filter = this.searchKey.trim().toLowerCase();
    }
  }

  viewMaps(atm: Atm) {
    this.dialog.open(MapComponent, {data: atm}).afterClosed().subscribe(value => {
    });
  }


  loadData(atmFilter: Atm) {
    this.atmService.getList().subscribe((result: Atm[]) => {
      this.atms = result;
      this.tableData = new MatTableDataSource(result);
      this.tableData.sort = this.sort;
      this.tableData.paginator = this.paginator;
    });
  }

  viewAtmDatails(atm: Atm) {
    this.dialog.open(AdvancedFilterComponent, {data: atm}).afterClosed().subscribe(value => {
    });
  }

}
