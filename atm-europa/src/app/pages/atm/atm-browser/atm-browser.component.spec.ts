import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtmBrowserComponent } from './atm-browser.component';

describe('AtmBrowserComponent', () => {
  let component: AtmBrowserComponent;
  let fixture: ComponentFixture<AtmBrowserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtmBrowserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtmBrowserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
