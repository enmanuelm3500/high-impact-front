import {Component, Inject} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {Atm} from '../../../core/models/atm';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-advanced-filter',
  templateUrl: './advanced-filter.component.html',
  styleUrls: ['./advanced-filter.component.css']
})
export class AdvancedFilterComponent {
  form: FormGroup;
  atm: Atm = new Atm();
  data: Atm = new Atm();
  hasUnitNumber = false;
  title = 'Advanced Filter';


  constructor(
    private dialogRef: MatDialogRef<AdvancedFilterComponent>,
    @Inject(MAT_DIALOG_DATA) data
  ) {
    this.data = data;
    this.form = this.createForm();
    if (this.data != null) {
      this.updateForm(this.data);
      this.title = 'Detalle ATM';
    }
  }

  updateForm(atm: Atm) {
    this.form.get('distance').setValue(atm.distance);
    this.form.get('type').setValue(atm.type);
    this.form.get('street').setValue(atm.address.street);
    this.form.get('houseNumber').setValue(atm.address.houseNumber);
    this.form.get('postalCode').setValue(atm.address.postalCode);
    this.form.get('city').setValue(atm.address.city);
    this.form.get('lng').setValue(atm.address.geoLocation.lng);
    this.form.get('lat').setValue(atm.address.geoLocation.lat);
    this.form.disable();
  }

  createForm() {
    return new FormGroup({
        distance: new FormControl(null),
        type: new FormControl(null),
        street: new FormControl(null),
        houseNumber: new FormControl(null),
        postalCode: new FormControl(null),
        city: new FormControl(null),
        lng: new FormControl(null),
        lat: new FormControl(null),
      }
    );
  }

  onSubmit() {
    if (this.form.valid) {
      Object.assign(this.atm, this.form.getRawValue());
      this.dialogRef.close(this.atm);
    }
  }

  close() {
    this.dialogRef.close();
  }


}
