import {RouterModule, Routes} from '@angular/router';
import {PagesComponent} from './pages.component';
import {HomeComponent} from './home/home.component';
import {AtmBrowserComponent} from './atm/atm-browser/atm-browser.component';


const pagesRoutes: Routes = [
  {
    path: '',
    component: PagesComponent,
    children: [
      {path: 'home', component: HomeComponent},
      {path: 'atm-browser', component: AtmBrowserComponent},

      {path: '', redirectTo: '/home', pathMatch: 'full'}
    ]
  },
];
export const PAGES_ROUTES = RouterModule.forChild(pagesRoutes);
