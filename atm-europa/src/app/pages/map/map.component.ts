import {Component, Inject, OnInit} from '@angular/core';
import {Atm} from '../../core/models/atm';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {

  data: Atm;

  title = 'titulo';
  position = {
    lat: 0,
    lng: 0
  };

  label = {
    color: 'black',
    text: 'marcador'
  };

  constructor(
    private dialogRef: MatDialogRef<MapComponent>,
    @Inject(MAT_DIALOG_DATA) data
  ) {
    this.data = data;
  }

  ngOnInit(): void {
    this.position.lat = Number(this.data.address.geoLocation.lat);
    this.position.lng = Number(this.data.address.geoLocation.lng);

  }

  closeModal() {
    this.dialogRef.close();
  }


}
