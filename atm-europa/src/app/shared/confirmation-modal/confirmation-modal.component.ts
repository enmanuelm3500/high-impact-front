import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
//import {ClientService} from "../../core/services/client.service";

@Component({
  selector: 'app-confirmation-modal',
  templateUrl: './confirmation-modal.component.html',
  styleUrls: ['./confirmation-modal.component.css']
})
export class ConfirmationModalComponent implements OnInit {

  data: any;

  constructor(
    private dialog: MatDialogRef<ConfirmationModalComponent>,
    //private clientService: ClientService,
    @Inject(MAT_DIALOG_DATA) data: any
  ) {
    this.data = data;
  }

  ngOnInit(): void {
  }

  deleteClient(){
    this.dialog.close(1);
  }

  close(){
    this.dialog.close();
  }

}
