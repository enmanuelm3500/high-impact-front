import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BrowserContainerComponent} from "./browser-container/browser-container.component";
import { ConfirmationModalComponent } from './confirmation-modal/confirmation-modal.component';
import {MatCardModule} from "@angular/material/card";
import {MatButtonModule} from "@angular/material/button";
import {LayoutModule} from "@angular/cdk/layout";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatIconModule} from "@angular/material/icon";
import {MatListModule} from "@angular/material/list";
import {MatTableModule} from "@angular/material/table";
import {MatSortModule} from "@angular/material/sort";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatTabsModule} from "@angular/material/tabs";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatGridListModule} from "@angular/material/grid-list";
import {MatMenuModule} from "@angular/material/menu";
import {MatSelectModule} from "@angular/material/select";
import {MatRadioModule} from "@angular/material/radio";
import {MatDialogModule} from "@angular/material/dialog";
import { NopagefoundComponent } from './nopagefound/nopagefound.component';



@NgModule({
  declarations: [BrowserContainerComponent, ConfirmationModalComponent, NopagefoundComponent],
  imports: [
    CommonModule,
    MatCardModule,
    MatButtonModule,
    LayoutModule,
    MatIconModule,

  ]
})
export class SharedModule { }
