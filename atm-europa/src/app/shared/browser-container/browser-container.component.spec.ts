import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrowserContainerComponent } from './browser-container.component';

describe('BrowserContainerComponent', () => {
  let component: BrowserContainerComponent;
  let fixture: ComponentFixture<BrowserContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrowserContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrowserContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
