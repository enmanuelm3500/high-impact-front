import {Component, Input, OnInit} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'app-browser-container',
  templateUrl: './browser-container.component.html',
  styleUrls: ['./browser-container.component.css']
})
export class BrowserContainerComponent<T> implements OnInit {

  @Input() name : string;
  @Input() tableData : MatTableDataSource<T>

  constructor() { }

  ngOnInit(): void {
  }

  applyFilter(filterValue: string){
    this.tableData.filter = filterValue.trim().toLocaleLowerCase();
  }

}
