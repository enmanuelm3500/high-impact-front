import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {UserLogin} from '../core/models/user-login';
import {UserSession} from '../core/models/user-session';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthenticationService} from '../core/services/authetication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  user: UserLogin;
  checkErrorCompany: boolean = false;
  checkError: boolean = false;

  mensajeError: string = '';

  constructor(
    private router: Router,
    private antheticationService: AuthenticationService
  ) {
    this.user = {
      username: '',
      password: '',
    };
  }

  ngOnInit(): void {


    if (localStorage.getItem('login')) {
      this.router.navigate(['/home']);
    }
    this.form = this.createForm();

  }

  createForm() {
    return new FormGroup({
        username: new FormControl('admin', [Validators.required]),
        password: new FormControl('12345', Validators.required)
      }
    );
  }

  login() {
    if (this.form.valid) {
      Object.assign(this.user, this.form.getRawValue());
      this.authentication(this.user);
    }
  }


  authentication(user: UserLogin) {
    this.antheticationService
      .authentication(user).subscribe(value => {
      if (value.status === 200) {
        let dataAuth: UserSession = {
          token: value.body.token,
          username: value.body.user
        };
        this.antheticationService.saveSession(dataAuth);
        this.router.navigate(['/home']);
      }
    });
  }

  passRecover() {
  }
}

